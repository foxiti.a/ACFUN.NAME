import './requests.dart';
import 'dart:async';

const ACFUN = "http://www.acfun.cn";

const ALIVEMSG = '<div class="alert alert-success" role="alert"><strong>完全OK！</strong> ACFUN可以正常访问！</div>';

const UNALIVEMSG = '<div class="alert alert-danger" role="alert"><strong>糟糕！</strong> ACFUN爆炸了！目前系统监测到无法正常访问！</div>';

const P = '{#state#}';

Future<bool> checkAcfunIsAlive() async{
    print("Checking acfun");
    Completer<bool> cpte = new Completer();
    get(ACFUN).then((Response res){
        if (res.raw.connectionInfo != null){
            cpte.complete(true);
        } else {
            cpte.complete(false);
        }
    }, onError: (_,__) => cpte.complete(false));
    return cpte.future;
}


Future<String> renderTemplate(String template) async{
    var msg;
    bool status = await checkAcfunIsAlive();
    if (status){
        msg = ALIVEMSG;
    } else {
        msg = UNALIVEMSG;
    }
    return template.replaceAll(P, msg);
}