import 'dart:io' show HttpClient,HttpClientRequest,HttpClientResponse,HttpHeaders;
import 'dart:convert' show JSON,UTF8;
import 'dart:async' show Future,Completer;


final HttpClient _client = new HttpClient();


typedef ProcessRequestCallback(Request request);


class Request{
    HttpClientRequest _raw;

    Request(this._raw);

    HttpHeaders get headers => raw.headers;

    HttpClientRequest get raw => _raw;

    void close() => raw.close();
}


class Response{
    HttpClientResponse _raw;

    Response(this._raw);

    HttpHeaders get headers => raw.headers;

    HttpClientResponse get raw => _raw;

    Future<String> get body => raw.transform(UTF8.decoder).join();

    Future<dynamic> fromJson() => body.then((String s) => JSON.decode(s));

    Future<dynamic> get json => fromJson();
}


Future<Response> get(String address,{ProcessRequestCallback callback}) async {
    Completer<Response> cpte = new Completer<Response>();
    HttpClientRequest rawReq = await _client.getUrl(Uri.parse(address));
    Request req = new Request(rawReq);
    callback(req);
    req.raw.close().timeout(new Duration(seconds: 5))
        .then((HttpClientResponse raw){
            cpte.complete(new Response(raw));
        }, onError: (e,st){
            cpte.completeError(e,st);
        });
    return cpte.future;
}


Future<Response> post(String address,{String data,Map<String, dynamic> json,ProcessRequestCallback callback}) async {
    Completer<Response> cpte = new Completer<Response>();
    HttpClientRequest rawReq = await _client.postUrl(Uri.parse(address));
    Request req = new Request(rawReq);
    callback(req);
    String rawData;
    if (data == null && json != null){
        rawData = JSON.encode(json);
    } else if (data != null){
        rawData = data;
    } else {
        rawData = "";
    }
    req.raw.write(rawData);
    req.raw.close()
        .then((HttpClientResponse raw){
            cpte.complete(new Response(raw));
        }, onError: (){
            cpte.completeError(new Exception("Could not finish request"));
        });
    return cpte.future;
}
