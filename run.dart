import './lib/acfun.dart';
import 'dart:io' show File;


const PAGEFILE = "./template.html";
const INDEXPAGE = "./index.html";


main() async{
    print("Creating");
    String t = await new File(PAGEFILE).readAsString();
    String h = await renderTemplate(t);
    await new File(INDEXPAGE).writeAsString(h);
    print("Static page created");
}